//
//  InspiringPeople.swift
//  Inspired
//
//  Created by Marko Vurnek on 29/03/2017.
//  Copyright © 2017 Marko Vurnek. All rights reserved.
//

import Foundation

class InspiringPeople{

    var id: String = ""
    var name: String = ""
    var year: String = ""
    var imageUrl: String = ""
    var description: String = ""
    var quote: String = ""
    
    init(id: String, name: String, year: String, imageUrl: String, description: String = "", quote: String = ""){
    
    self.id = id
    self.name=name
    self.year=year
    self.imageUrl=imageUrl
    self.description=description
    self.quote=quote
    
    }
    

}
