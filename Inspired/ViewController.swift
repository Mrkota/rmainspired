//
//  ViewController.swift
//  Inspired
//
//  Created by Marko Vurnek on 29/03/2017.
//  Copyright © 2017 Marko Vurnek. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var inspireMe: [InspiringPeople] = []
    @IBOutlet var mainTableView: UITableView!
    @IBOutlet var switchLabel: UILabel!
    @IBOutlet var imageClick: UIButton!
    
    
    
    var mostInspiring = [InspiringPeople(id: "1",
                                         name: "Steve Wozniak",
                                         year: "August 11, 1950",
                                         
                                         imageUrl: "https://s-media-cache-ak0.pinimg.com/originals/c3/12/15/c312150de0ac392ac45c74c203fdc9dd.jpg",
                                         
                                         description: "American inventor, electronics engineer, programmer, and technology entrepreneur who co-founded Apple Inc. He is known as a pioneer of the personal computer revolution of the 1970s and 1980s, along with Apple co-founder Steve Jobs.",
                                         
                                         quote: "Never trust a computer you can't throw out a window."),
                         
                         InspiringPeople(id: "2",
                                         name: "Aaron Hillel Swartz",
                                         year: "November 8, 1986 – January 11, 2013",
                                         
                                         imageUrl: "https://upload.wikimedia.org/wikipedia/commons/0/06/Aaron_Swartz_profile.jpg",
                                         
                                         description: "American computer programmer, entrepreneur, writer, political organizer, and Internet hacktivist. He was involved in the development of the web feed format RSS and the Markdown publishing format, the organization Creative Commons, the website framework web.py, and the social news site Reddit, in which he became a partner after its merger with his company, Infogami.",
                                         quote: "Assume nobody else has any idea what they're doing, either."),
    
                         
                         InspiringPeople(
                                         id:"3",
                                         name: "Dennis MacAlistair Ritchie",
                                         year: "September 9, 1941 – October 12, 2011",
                                         
                                         imageUrl: "https://officechai.com/wp-content/uploads/2016/10/dennis-ritchie.jpg",
                                         
                                         description: "American computer scientist whoe created the C programming language and, with long-time colleague Ken Thompson, the Unix operating system. Ritchie and Thompson received the Turing Award from the ACM in 1983, the Hamming Medal from the IEEE in 1990 and the National Medal of Technology from President Bill Clinton in 1999. Ritchie was the head of Lucent Technologies System Software Research Department when he retired in 2007. He was the  in K&R C, and commonly known by his username dmr.",
                                         quote: "I'm not a person who particularly had heros when growing up.")
                         ]
    

    
    @IBAction func toogleDarkMode(_ sender: Any) {
        
        let darkModeSwitch = sender as! UISwitch
        if darkModeSwitch.isOn {
            
            view.backgroundColor=UIColor.darkGray
            
          
            
            switchLabel.text="Normal mode"
        }else{
            view.backgroundColor=UIColor.white
          
           
           
            switchLabel.text="Night mode"
        }
    }
    

    
    
    
   
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.backgroundColor=UIColor.clear
        

        
        return inspireMe.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "customCell", for: indexPath) as! CustomTableViewCell
        let idx: Int = indexPath.row
        cell.inspireName?.text = inspireMe[idx].name
        cell.inspireYear?.text = inspireMe[idx].year
        cell.inspireDescription?.text = inspireMe[idx].description
      // var send = inspireMe[idx].quote
   
     
        
        cell.backgroundColor=UIColor.clear
        
        
      
        
        
        displayInspiringImage(idx, cell: cell)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let idx: Int = indexPath.row
        
        
        let ispis = mostInspiring[idx].quote
        
        let printQuote = UIAlertController(title: "Quote!", message: ispis, preferredStyle: .alert)
        
        let exitQuote = UIAlertAction(title: "Exit", style: .default, handler: {
            
            action in
            print("Exited ...")
            
        })
        
        printQuote.addAction(exitQuote)
        
        present(printQuote, animated: true, completion: nil)
    }
    

    
    func displayInspiringImage(_ row: Int, cell: CustomTableViewCell){
        let url: String = (URL(string: inspireMe[row].imageUrl)?.absoluteString)!
        URLSession.shared.dataTask(with: URL(string: url)!, completionHandler:{(data, response, error) -> Void in
            if error != nil {
                
                print(error!)
                return
                
            }
            
            DispatchQueue.main.async(execute :{
                
                let image = UIImage(data: data!)
                cell.inspireImageView?.image=image
               
                
            })
            
        }).resume()
        
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        mainTableView.reloadData()
        
        if inspireMe.count == 0 {
        
      for i in 0 ..< 3{
        inspireMe.append(mostInspiring[i])
            }
        
        }
    }
    

    override func viewDidLoad() {
        
    
        super.viewDidLoad()
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

