//
//  CustomTableViewCell.swift
//  Inspired
//
//  Created by Marko Vurnek on 29/03/2017.
//  Copyright © 2017 Marko Vurnek. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {
    
    @IBOutlet var inspireImageView: UIImageView!
    @IBOutlet var inspireName: UILabel!
    @IBOutlet var inspireYear: UILabel!
    @IBOutlet var inspireDescription: UILabel!
    @IBOutlet var imageCustom: UIButton!
    
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
